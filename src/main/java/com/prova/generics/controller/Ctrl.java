package com.prova.generics.controller;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.prova.generics.model.Offer;
import com.prova.generics.service.IGenericService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class Ctrl 
{
	private final IGenericService<Offer> genericService;
	
	@GetMapping("offers")
	public ResponseEntity<List<Offer>> getOffers()
	{
		return new ResponseEntity<List<Offer>>(genericService.findAll(),HttpStatus.OK);
	}

	@GetMapping("offers/{id}")
	public ResponseEntity<Offer> getOfferByObjectId(@PathVariable("id") String id)
	{
		try 
		{
			return new ResponseEntity<Offer>(genericService.findById(id),HttpStatus.OK);
		}
		catch(NoSuchElementException e)
		{
			return new ResponseEntity<Offer>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("offers")
	public ResponseEntity<Offer> saveOffer(@RequestBody Optional<Offer> offer)
	{
		try
		{
			return new ResponseEntity<Offer>(genericService.save(offer),HttpStatus.OK);
		}
		catch(NoSuchElementException e)
		{
			return new ResponseEntity<Offer>(HttpStatus.BAD_REQUEST);
		}
	}

}
