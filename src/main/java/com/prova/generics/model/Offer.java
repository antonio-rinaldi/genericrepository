package com.prova.generics.model;

import javax.validation.constraints.NotNull;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document
public class Offer implements Generic
{
	@Id
	private ObjectId _id;
	
	@Field
	@NotNull
	@Indexed(unique= true)
	private PromotionType promotionType;
	
	@Field
	@NotNull
	private double price; 
	
	@Field
	@NotNull
	private int freeMinutes;

	
	public Offer() {}
	
	public Offer(PromotionType promotionType, double price, int freeMinutes)
	{
		this.promotionType=promotionType;
		this.price = price;
		this.freeMinutes = freeMinutes;
	}
	
	public double getPrice() 
	{
		return price;
	}

	public void setPrice(double price) 
	{
		this.price = price;
	}

	public int getFreeMinutes()
	{
		return freeMinutes;
	}

	public void setFreeMinutes(int freeMinutes) 
	{
		this.freeMinutes = freeMinutes;
	}

//	@JsonIgnore
	public PromotionType getPromotionType() {
		return promotionType;
	}

	public void setPromotionType(PromotionType promotionType) {
		this.promotionType = promotionType;
	}
		
}
