package com.prova.generics.model;

public enum PromotionType 
{
	NO_PROMOTION,
	NEW_CUSTOMER,
	NO_PROMOTION_OLD_CUSTOMER,
	PROMOTION_OLD_CUSTOMER
}
