package com.prova.generics.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.prova.generics.model.Generic;

@Repository
public interface GenericRepository<T extends Generic> extends MongoRepository<T, ObjectId>
{

}
