package com.prova.generics.service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import com.prova.generics.model.Generic;
import com.prova.generics.repository.GenericRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class GenericService<T extends Generic> implements IGenericService<T>
{
	private final GenericRepository<T> genericRepo;
	
	public T save(Optional<T> object) throws NoSuchElementException
	{
		return genericRepo.save(object.orElseThrow());
	}
	
	public List<T> findAll()
	{
		return genericRepo.findAll();
	}

	@Override
	public T findById(String id) throws NoSuchElementException
	{
		return genericRepo.findById(new ObjectId(id)).orElseThrow();
	}
}
