package com.prova.generics.service;

import java.util.List;
import java.util.Optional;

public interface IGenericService<T> 
{
	
	public T save(Optional<T> object);
	public T findById(String id);
	public List<T> findAll();
	
}

